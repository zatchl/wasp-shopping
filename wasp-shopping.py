import csv

def wasps_on_nests_matrix():
    with open("wasp-shopping.csv", newline='') as wasp_shopping_data:
        wasp_csv_reader = csv.reader(wasp_shopping_data, quotechar='"')
        for nest_id, nest_row in enumerate(wasp_csv_reader):
            if nest_id == 0:
                continue
            for wasps_on_nest in [cell.split(',') for cell in nest_row if len(cell) >= 4]:
                yield (wasps_on_nest, nest_id)

def write_wasp_dictionary_to_file(wasp_dictionary, filename):
    with open(filename, 'w', newline='') as wasp_file:
        wasp_csv_writer = csv.writer(wasp_file)
        for wasp, values in wasp_dictionary.items():
            wasp_csv_writer.writerow([wasp, str(sorted(values)), len(values)])

def unique_nests():
    wasp_nest_dictionary = dict()
    for wasps_on_nest, nest_id in wasps_on_nests_matrix():
        for wasp in wasps_on_nest:
            if wasp not in wasp_nest_dictionary:
                wasp_nest_dictionary[wasp] = set()
            wasp_nest_dictionary[wasp].add(nest_id)

    write_wasp_dictionary_to_file(wasp_nest_dictionary, "unique-nests.csv")

def unique_nestmates():
    wasp_nestmate_dictionary = dict()
    for wasps_on_nest, _ in wasps_on_nests_matrix():
        for wasp in wasps_on_nest:
            if wasp not in wasp_nestmate_dictionary:
                wasp_nestmate_dictionary[wasp] = set()
            for nestmate in [other_wasp for other_wasp in wasps_on_nest if other_wasp != wasp]:
                wasp_nestmate_dictionary[wasp].add(nestmate)

    write_wasp_dictionary_to_file(wasp_nestmate_dictionary, "unique-nestmates.csv")

if __name__ == "__main__":
    unique_nests()
    unique_nestmates()
